require 'turnip'
require 'turnip/capybara'
require 'turnip/rspec'
require 'capybara'
require 'capybara/poltergeist'

## mobile の useragent
#Capybara.register_driver :mobile do |app|
#  Capybara::RackTest::Driver.new(app, :headers => {'HTTP_USER_AGENT' => 'hogehoge mobile browser'})
#end
#
## pc の useragent
#Capybara.register_driver :pc do |app|
#  Capybara::RackTest::Driver.new(app, :headers => {'HTTP_USER_AGENT' => 'hogehoge PC browser'})
#end

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, {
    js_errors: false,
    default_wait_time: 30,
    timeout: 100,
    window_size: [600, 900],
    debug: true
  })
end

Capybara.configure do |config|
  config.default_driver = :pc
  config.javascript_driver = :poltergeist
  config.ignore_hidden_elements = true
  config.default_wait_time = 30
end

Dir.glob("spec/**/*steps.rb") { |f| load f, true }




