# encoding: utf-8
# language: ja

機能: 指定したURLのスクリーンショットを保存
  シナリオ: 画面表示をして複数のスクリーンショットを保存する
    もし "http://www.cocos2d-x.org/MoonWarriors/index.html" を表示
    かつ "5" 秒待つ
    かつ "スタートボタン" をクリック
    ならば "矩形A" のスクリーンショットを撮って "screen_shot_A.png" に保存
    ならば "矩形B" のスクリーンショットを撮って "screen_shot_B.png" に保存
    ならば "画面全体" のスクリーンショットを撮って "screen_shot_whole.png" に保存


