# encoding: utf-8

#require 'capybara-webkit'
require 'capybara/poltergeist'
require 'logger'
require './lib/screen_rect'
require './lib/rect_definition'

log = Logger.new(STDOUT)
Capybara.default_driver = :poltergeist

step %(:url を表示) do |url|
  visit url
end

step %(:sec 秒待つ) do |sec|
  sleep sec.to_i
end



step %(:place をクリック) do |place|
  rect = ScreenRect.new(page, 'button_rect').addTo(RectDefinition.new.send(place)).style('backgroundColor', 'rgba(125,0,0,0.5)')
	rect.dissmiss
  page.driver.click(rect.x + rect.w / 2, rect.y + rect.h / 2);
end

step %(:place のスクリーンショットを撮って :png_name に保存) do |place, png_name|
  rect = ScreenRect.new(page, 'clip_rect').addTo(RectDefinition.new.send(place))
  save_screenshot "screen_shot/#{png_name}", selector: "##{rect.elem_id}"
	rect.dissmiss
end


