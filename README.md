任意の矩形のスクリーンショットを撮る


## setup

```
$ npm install -g phantomjs
$ bundle install --path vendor/bundle
```


## how to take screenshot

```
$ bundle exec rake
```

## write features

```
$ $EDITOR spec/features/screen_shot.feature
```

## define steps

```
$ $EDITOR spec/steps/screen_shot_steps.rb
```

## define the rect for tap
```
$ EDITOR lib/rect_definition.rb
```
