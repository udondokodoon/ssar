class ScreenRect
  attr_reader :elem_id, :x, :y, :w, :h
  def initialize(page, id)
    @page = page
    @elem_id = id;
    @rect_tmpl =<<-JS
      (function(elem_id, left_, top_, width_, height_) {
        var r = document.getElementById(elem_id);
        if (!r) {
          r = document.createElement('div');
          r.id = elem_id;
          document.body.appendChild(r);
        }
        with(r.style) {
          display = 'block';
          position = 'absolute';
          left = left_;
          top = top_;
          width = width_;
          height = height_;
        };
        r.onclick = function() {
          this.style.border = '2px solid yellow';
        };
      })('%s', '%s', '%s', '%s', '%s');
    JS
  end

  def dissmiss
    cmd =<<-JS
      (function(elem_id) {
        document.getElementById(elem_id).style.display = 'none';
      })('%s');
    JS
    @page.execute_script sprintf(cmd, @elem_id)
  end

  def style(key, value)
    cmd =<<-JS
      (function(elem_id, key, value) {
        var r = document.getElementById(elem_id);
        r.style[key] = value;
      })('%s', '%s', '%s')
    JS
    @page.execute_script sprintf(cmd, @elem_id, key, value)
    self
  end


  def addTo(rect)
    @page.execute_script sprintf(@rect_tmpl, @elem_id, rect[:x], rect[:y], rect[:w], rect[:h]);
    @x = rect[:x].sub(/px/, '').to_i
    @y = rect[:y].sub(/px/, '').to_i
    @w = rect[:w].sub(/px/, '').to_i
    @h = rect[:h].sub(/px/, '').to_i
    self
  end
end


